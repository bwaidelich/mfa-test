<?php
namespace Wwwision\MfaTest\Controller;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use Neos\Flow\Security\Context;

class StandardController extends ActionController
{
    /**
     * @Flow\Inject
     * @var Context
     */
    protected $securityContext;

    public function indexAction()
    {
        $this->view->assign('roles', $this->securityContext->getRoles());
    }
}
