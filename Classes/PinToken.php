<?php
namespace Wwwision\MfaTest;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\ActionRequest;
use Neos\Flow\Security\Authentication\Token\AbstractToken;
use Neos\Utility\ObjectAccess;

class PinToken extends AbstractToken
{
    /**
     * @var array
     * @Flow\Transient
     */
    protected $credentials = ['pin' => ''];

    public function updateCredentials(ActionRequest $actionRequest)
    {
        $httpRequest = $actionRequest->getHttpRequest();
        if ($httpRequest->getMethod() !== 'POST') {
            return;
        }

        $arguments = $actionRequest->getInternalArguments();
        $pin = ObjectAccess::getPropertyPath($arguments, '__authentication.Wwwision.MfaTest.pin');

        if (!empty($pin)) {
            $this->credentials['pin'] = $pin;
            $this->setAuthenticationStatus(self::AUTHENTICATION_NEEDED);
        }
    }
}
