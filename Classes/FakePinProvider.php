<?php
namespace Wwwision\MfaTest;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Security\AccountFactory;
use Neos\Flow\Security\Authentication\Provider\AbstractProvider;
use Neos\Flow\Security\Authentication\Token\UsernamePassword;
use Neos\Flow\Security\Authentication\Token\UsernamePasswordHttpBasic;
use Neos\Flow\Security\Authentication\TokenInterface;
use Neos\Flow\Security\Context;
use Neos\Flow\Security\Exception\AccessDeniedException;
use Neos\Flow\Security\Exception\UnsupportedAuthenticationTokenException;
use Neos\Flow\Security\Policy\PolicyService;

class FakePinProvider extends AbstractProvider
{
    /**
     * @Flow\Inject
     * @var Context
     */
    protected $securityContext;

    /**
     * @Flow\Inject
     * @var PolicyService
     */
    protected $policyService;

    public function getTokenClassNames()
    {
        return [PinToken::class];
    }

    public function authenticate(TokenInterface $authenticationToken)
    {
        if (!($authenticationToken instanceof PinToken)) {
            throw new UnsupportedAuthenticationTokenException('This provider cannot authenticate the given token.', 1484659029);
        }
        $credentials = $authenticationToken->getCredentials();
        if (!is_array($credentials) || !isset($credentials['pin'])) {
            $authenticationToken->setAuthenticationStatus(TokenInterface::NO_CREDENTIALS_GIVEN);
            return;
        }

        $authenticatedAccount = $this->securityContext->getAccountByAuthenticationProviderName('DefaultProvider');
        if ($authenticatedAccount === null) {
            throw new AccessDeniedException('No authenticated user. Tried to apply MFA via PIN when no user was authenticated first');
        }

        if ($credentials['pin'] !== '1234') {
            $authenticationToken->setAuthenticationStatus(TokenInterface::WRONG_CREDENTIALS);
            return;
        }
        $authenticatedAccount->addRole($this->policyService->getRole('Wwwision.MfaTest:Mfa'));
        $authenticationToken->setAuthenticationStatus(TokenInterface::AUTHENTICATION_SUCCESSFUL);
        $authenticationToken->setAccount($authenticatedAccount);
    }
}
